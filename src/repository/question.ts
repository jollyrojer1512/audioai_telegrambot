import { inject, injectable } from "inversify";
import { Symbols } from "../dependencies/symbols";
import { Db } from "../components/db/db";
import { BaseRepository } from "./base";
import { Question, QuestionCategory } from "../components/models/question";
import { QuestionEntity } from "../domain/entities/question";

export interface QuestionRepository {
    create(model: Question): Promise<QuestionEntity>;
    updateOne(_id: string, $set: any): Promise<void>;
    getById(_id: string): Promise<QuestionEntity>;
    getByIndex(index: number): Promise<QuestionEntity>;
    getNextByIndexAndType(index: number, category: QuestionCategory): Promise<QuestionEntity>;
}

@injectable()
export class QuestionRepositoryImpl extends BaseRepository<Question, QuestionEntity> implements QuestionRepository {
    constructor(@inject(Symbols.Infrastructure.DB) private readonly db: Db) {
        super();
        super.init(this.db.main.question, QuestionEntity);
    }

    async create(model: Question): Promise<QuestionEntity> {
        return super.create(model);
    }

    async updateOne(_id: string, $set: any): Promise<void> {
        await super.update({ _id }, { $set });
    }

    async getById(_id: string): Promise<QuestionEntity> {
        const question = await super.getOne({ _id });
        return question;
    }

    async getByIndex(index: number): Promise<QuestionEntity> {
        const question = await super.getOne({ index });
        return question;
    }

    async getNextByIndexAndType(index: number, category: QuestionCategory): Promise<QuestionEntity> {
        const categories = this.getCategories(category);
        const question = await super.getOne({ index: { $gt: index }, category: { $in: categories } });
        return question;
    }

    private getCategories(category: QuestionCategory): QuestionCategory[] {
        const all: QuestionCategory[] = [QuestionCategory.russianLanguage, QuestionCategory.arithmetic];

        if (category === QuestionCategory.all) return all;
        return [category];
    }
}
