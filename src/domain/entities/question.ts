import { BaseEntity } from "./baseEntity";
import { Question, QuestionCategory, QuestionObject } from "../../components/models/question";

export class QuestionEntity extends BaseEntity<Question> {
    get index(): number {
        return this.model.index;
    }
    set index(value: number) {
        this.model.index = value;
    }

    get registerTime(): Date {
        return this.model.register_time;
    }
    set registerTime(value: Date) {
        this.model.register_time = value;
    }

    get category(): QuestionCategory {
        return this.model.category;
    }
    set category(value: QuestionCategory) {
        this.model.category = value;
    }

    get question(): QuestionObject {
        return this.model.question;
    }
    set question(value: QuestionObject) {
        this.model.question = value;
    }
}
