import { inject, injectable } from "inversify";
import { Symbols } from "../dependencies/symbols";
import { Db } from "../components/db/db";
import { BaseRepository } from "./base";
import { Quiz } from "../components/models/quiz";
import { QuizEntity } from "../domain/entities/quiz";

export interface QuizRepository {
    create(test: Quiz): Promise<QuizEntity>;
    getById(_id: string): Promise<QuizEntity>;
    getByUserId(userId: number): Promise<QuizEntity>;
    updateOne(_id: string, $set: any): Promise<void>;
}

@injectable()
export class QuizRepositoryImpl extends BaseRepository<Quiz, QuizEntity> implements QuizRepository {
    constructor(@inject(Symbols.Infrastructure.DB) private readonly db: Db) {
        super();
        super.init(this.db.main.test, QuizEntity);
    }

    async create(model: Quiz): Promise<QuizEntity> {
        return super.create(model);
    }

    async updateOne(_id: string, $set: any): Promise<void> {
        await super.update({ _id }, { $set });
    }

    async getById(_id: string): Promise<QuizEntity> {
        const test = super.getOne({ _id });
        return test;
    }

    async getByUserId(user_id: number): Promise<QuizEntity> {
        const test = await super.getOne({ user_id: user_id.toString() });
        return test;
    }
}
