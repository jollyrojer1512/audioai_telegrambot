import { userSchema } from "./user";
import { Model, Schema } from "mongoose";
import { questionSchema } from "./question";
import { quizSchema } from "./quiz";
import { messageSchema } from "./message";

export type ExtractGeneric<Type> = Type extends Schema<infer X, Model<infer X, any, any>, {}> ? X : never;

export type SchemaToModel<T extends object> = {
    [K in keyof T]: Model<ExtractGeneric<T[K]>, {}>;
};

export const DBModels = {
    main: {
        user: userSchema,
        question: questionSchema,
        test: quizSchema,
        message: messageSchema,
    },
};
