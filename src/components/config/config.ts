import * as dotenv from "dotenv";
import { injectable } from "inversify";

type DBNames = {
    main: string;
};

type Redis = {
    host: string;
    port: number;
    db: number;
};

@injectable()
export class Config {
    private readonly _botToken: string;
    private readonly _dbUrl: string;

    private readonly _dbNames: DBNames;

    private readonly _redis: Redis;
    private readonly _channel: string;

    constructor() {
        dotenv.config();
        const env = process.env;

        this._botToken = env["BOT_TOKEN"];
        this._dbUrl = env["DB_URI"];

        this._dbNames = {
            main: env["DB_NAME_MAIN"],
        };

        this._redis = {
            host: env["REDIS_HOST"],
            port: parseInt(env["REDIS_PORT"]),
            db: parseInt(env["REDIS_DB"]),
        };

        this._channel = env["AUDIO_AI_CHANNEL"];
    }

    get botToken(): string {
        return this._botToken;
    }
    get dbUrl(): string {
        return this._dbUrl;
    }
    get dbNames(): DBNames {
        return this._dbNames;
    }
    get redis(): Redis {
        return this._redis;
    }
    get channel(): string {
        return this._channel;
    }
}
