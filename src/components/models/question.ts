import { Schema, SchemaTypes } from "mongoose";

export enum QuestionCategory {
    all = "all",
    arithmetic = "arithmetic",
    russianLanguage = "russian language",
}
export enum QuestionType {
    all = "all",
    text = "text",
    audio = "audio",
    photo = "photo",
    video = "video",
}

export type QuestionAnswerType = string;

export type QuestionObject = {
    type: QuestionType;
    value: string;
    answer: QuestionAnswerType;
    points: number;
};

export interface Question {
    category: QuestionCategory;
    question: QuestionObject;
    register_time: Date;
    index: number;
}

export const questionSchema = new Schema<Question>({
    index: { type: Number },
    register_time: { type: Date },
    category: { type: String },
    question: { type: SchemaTypes.Mixed },
});
