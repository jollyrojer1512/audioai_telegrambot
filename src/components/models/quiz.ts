import { Schema, SchemaTypes } from "mongoose";
import { QuestionCategory } from "./question";

export type QuizPeriod = {
    start: Date;
    end?: Date;
};

export type QuizQuestions = {
    category: QuestionCategory;
    index: number;
    correct: number;
    wrong: number;
};

export interface Quiz {
    user_id: string;
    points: number;
    period: QuizPeriod;
    questions: QuizQuestions;
}

export const quizSchema = new Schema<Quiz>({
    user_id: { type: String },
    points: { type: Number },
    period: { type: SchemaTypes.Mixed },
    questions: { type: SchemaTypes.Mixed },
});
