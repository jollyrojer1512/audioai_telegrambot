import { inject, injectable } from "inversify";
import { Symbols } from "../dependencies/symbols";
import { QuizEntity } from "../domain/entities/quiz";
import { QuizRepository } from "../repository/quiz";
import { Quiz } from "../components/models/quiz";
import { QuestionCategory } from "../components/models/question";

export interface QuizService {
    create(userId: number): Promise<QuizEntity>;
    getById(id: string): Promise<QuizEntity>;
    getByUserId(userId: number): Promise<QuizEntity>;
    updateOne(quiz: QuizEntity): Promise<QuizEntity>;
    setIndexByUserId(userId: number, index: number): Promise<QuizEntity>;
}

@injectable()
export class QuizServiceImpl implements QuizService {
    constructor(@inject(Symbols.Repository.Quiz) private readonly quizRepository: QuizRepository) {}

    async create(userId: number): Promise<QuizEntity> {
        const test = this.createQuizObject(userId);
        return await this.quizRepository.create(test);
    }

    async getById(id: string): Promise<QuizEntity> {
        const test = await this.quizRepository.getById(id);
        if (!test) return undefined;
        return test;
    }
    async getByUserId(userId: number): Promise<QuizEntity> {
        const test = await this.quizRepository.getByUserId(userId);
        if (!test) return this.create(userId);
        return test;
    }

    async updateOne(quiz: QuizEntity): Promise<QuizEntity> {
        await this.quizRepository.updateOne(quiz.id, await quiz.toObject());
        return quiz;
    }
    async setIndexByUserId(userId: number, index: number): Promise<QuizEntity> {
        const test = await this.getByUserId(userId);
        test.questions.index = index;
        test.period.end = new Date();
        return await this.updateOne(test);
    }

    private createQuizObject(userId: number, category: QuestionCategory = QuestionCategory.all): Quiz {
        return {
            user_id: userId.toString(),
            period: {
                start: new Date(),
                end: new Date(),
            },
            points: 0,
            questions: {
                category,
                index: 0,
                correct: 0,
                wrong: 0,
            },
        };
    }
}
