import { inject, injectable } from "inversify";
import { Symbols } from "../dependencies/symbols";
import { Db } from "../components/db/db";
import { BaseRepository } from "./base";
import { Message } from "../components/models/message";
import { MessageEntity } from "../domain/entities/message";

export interface MessageRepository {
    create(message: Message): Promise<MessageEntity>;
    getByUserId(userId: number): Promise<MessageEntity>;
    updateOne(message_id: number, $set: any): Promise<void>;
}

@injectable()
export class MessageRepositoryImpl extends BaseRepository<Message, MessageEntity> implements MessageRepository {
    constructor(@inject(Symbols.Infrastructure.DB) private readonly db: Db) {
        super();
        super.init(this.db.main.message, MessageEntity);
    }

    async create(model: Message): Promise<MessageEntity> {
        return super.create(model);
    }

    async updateOne(message_id: number, $set: any): Promise<void> {
        await super.update({ message_id: message_id.toString() }, { $set });
    }

    async getByUserId(id: number): Promise<MessageEntity> {
        const user = await super.getOne({ "from.id": id });
        return user;
    }
}
