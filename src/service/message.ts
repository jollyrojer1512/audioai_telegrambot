import { inject, injectable } from "inversify";
import { Symbols } from "../dependencies/symbols";
import { MessageEntity } from "../domain/entities/message";
import { Message } from "../components/models/message";
import { MessageRepository } from "../repository/message";
import { GrammyMessage } from "../components/util/types";

export interface MessageService {
    create(message: GrammyMessage): Promise<MessageEntity>;
    getByUserId(id: number): Promise<MessageEntity>;
}

@injectable()
export class MessageServiceImpl implements MessageService {
    constructor(@inject(Symbols.Repository.Message) private readonly messageRepository: MessageRepository) {}

    async create(grammyMessage: GrammyMessage): Promise<MessageEntity> {
        const message = this.createMessageObject(grammyMessage);
        const messageId = parseInt(message.message_id);
        const entity = await this.getByUserId(messageId);
        if (!entity) return await this.messageRepository.create(message);
        return await this.updateOne(entity);
    }

    async getByUserId(id: number): Promise<MessageEntity> {
        const user = await this.messageRepository.getByUserId(id);
        if (!user) return undefined;
        return user;
    }

    private createMessageObject(msg: GrammyMessage): Message {
        return {
            message_id: msg.message_id.toString(),
            register_time: new Date(),
            chat: msg.chat,
            from: msg.from,
            text: msg.text,
            voice: msg.voice,
        };
    }
    private async updateOne(message: MessageEntity): Promise<MessageEntity> {
        return await message.save();
    }
}
