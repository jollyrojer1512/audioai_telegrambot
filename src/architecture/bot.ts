import { inject, injectable } from "inversify";
import { Symbols } from "../dependencies/symbols";
import { Config } from "../components/config/config";
import { Context, Bot as Grammy, Composer } from "grammy";
import { PrivateChat } from "./privateChat";
import { MessageService } from "../service/message";
import { MemoryService } from "../service/memory";

export interface Bot {
    bot: Grammy;
    privateChat: Composer<Context>;
    start(ctx: Context): Promise<void>;
    on(ctx: Context): Promise<void>;
    menu(ctx: Context): Promise<void>;
    audio(ctx: Context): Promise<void>;
}

@injectable()
export class BotImpl implements Bot {
    readonly bot: Grammy;
    readonly privateChat: Composer<Context>;

    constructor(
        @inject(Symbols.Infrastructure.Config) private config: Config,
        @inject(Symbols.Architecture.PrivateChat) private readonly privateChatCore: PrivateChat,
        @inject(Symbols.Service.Message) private readonly messageService: MessageService
    ) {
        this.bot = new Grammy(config.botToken);
        this.bot.start();
        this.privateChat = this.bot.filter((ctx) => ctx.chat?.type === "private");
    }

    async start(ctx: Context): Promise<void> {
        await this.messageService.create(ctx.message);
        const result = await this.privateChatCore.greeting(ctx.from);
        await ctx.reply(result.text, result.data);
        // await this.menu(ctx);
    }

    async on(ctx: Context): Promise<void> {
        await this.messageService.create(ctx.message);
    }

    async audio(ctx: Context): Promise<void> {
        await this.messageService.create(ctx.message);
        await this.privateChatCore.audio(ctx);
    }

    async menu(ctx: Context): Promise<void> {
        await this.privateChatCore.menu(ctx);
    }
}
