import { ContainerModule, interfaces } from "inversify";
import { Symbols } from "../symbols";
import { Config } from "../../components/config/config";
import { Db, DbImpl } from "../../components/db/db";

export const InfrastuctureContainerModule = new ContainerModule((bind: interfaces.Bind) => {
    bind<Config>(Symbols.Infrastructure.Config).to(Config).inSingletonScope();
    bind<Db>(Symbols.Infrastructure.DB).to(DbImpl).inSingletonScope();
});
