import { inject, injectable } from "inversify";
import { DBModels, SchemaToModel } from "../models/models";
import { Symbols } from "../../dependencies/symbols";
import { Config } from "../config/config";
import { DbFactory, DbFactoryImpl } from "./dbFactory";
import ioredis, { Redis } from "ioredis";

export type Memory = Redis;

export interface Db {
    main: SchemaToModel<typeof DBModels.main>;
    memory: Memory;
    init(): Promise<void>;
    closeAll(): Promise<void>;
}

@injectable()
export class DbImpl implements Db {
    main: SchemaToModel<typeof DBModels.main>;
    memory: Memory;

    private readonly factories: DbFactory[];

    constructor(@inject(Symbols.Infrastructure.Config) private readonly config: Config) {
        this.factories = [];
    }

    async init(): Promise<void> {
        await this.connectVitamix();
        await this.connectMemory();
    }

    async closeAll(): Promise<void> {
        for (const factory of this.factories) {
            await factory.disconnect();
        }
        // await this.memory.disconnect();
    }

    private async connectVitamix(): Promise<void> {
        const mainFactory = new DbFactoryImpl(this.config.dbUrl, this.config.dbNames.main);
        await mainFactory.connect();
        this.main = mainFactory.link(DBModels.main);
        this.factories.push(mainFactory);
    }

    private async connectMemory(): Promise<void> {
        const redis = this.config.redis;
        this.memory = new ioredis(redis.port, redis.host, { db: redis.db });
    }
}
