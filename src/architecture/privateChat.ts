import { inject, injectable } from "inversify";

import { Symbols } from "../dependencies/symbols";

import { Context } from "grammy";

import { TextService } from "../service/text";
import { KeyboardAnswer, KeyboardService } from "../service/keyboard";
import { UserService } from "../service/user";
import { GrammyUser } from "../components/util/types";
import { QuizService } from "../service/quiz";
import { MemoryService } from "../service/memory";

export interface PrivateChat {
    greeting(user: GrammyUser): Promise<KeyboardAnswer>;
    quiz(user: GrammyUser): Promise<KeyboardAnswer>;
    audio(ctx: Context): Promise<void>;
    menu(ctx: Context): Promise<void>;
    settings(ctx: Context): Promise<void>;
}

@injectable()
export class PrivateChatImpl implements PrivateChat {
    constructor(
        @inject(Symbols.Service.User) private readonly userService: UserService,
        @inject(Symbols.Service.Quiz) private readonly quizService: QuizService,
        @inject(Symbols.Service.Text) private readonly text: TextService,
        @inject(Symbols.Service.Keyboard) private readonly keyboard: KeyboardService,
        @inject(Symbols.Service.Memory) private readonly memory: MemoryService
    ) {}

    async greeting(user: GrammyUser): Promise<KeyboardAnswer> {
        await this.userService.create(user);
        return await this.keyboard.startQuiz();
    }

    async quiz(user: GrammyUser): Promise<KeyboardAnswer> {
        const quiz = await this.quizService.getByUserId(user.id);
        const keyboard = this.keyboard.question(quiz.questions.index, quiz.questions.category);
        return keyboard;
    }
    async audio(ctx: Context): Promise<void> {
        await this.memory.publishAudio(ctx);
    }

    async menu(ctx: Context): Promise<void> {
        await this.keyboard.menu(ctx);
    }
    async settings(ctx: Context): Promise<void> {
        await this.keyboard.settings(ctx);
    }
}
