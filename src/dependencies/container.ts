import { Container as InversifyContainer } from "inversify";
import { InfrastuctureContainerModule } from "./containers/infrastructure";
import { ArchitectureContainerModule } from "./containers/architecture";
import { Db } from "../components/db/db";
import { Symbols } from "./symbols";
import { ServiceContainerModule } from "./containers/service";
import { RepositoryContainerModule } from "./containers/repository";

export class Container {
    static Services = new InversifyContainer();

    static loadAll(): void {
        Container.Services.load(InfrastuctureContainerModule);
        Container.Services.load(ArchitectureContainerModule);
        Container.Services.load(ServiceContainerModule);
        Container.Services.load(RepositoryContainerModule);
    }

    static async init(): Promise<void> {
        console.log("Starting the world!");
        const db = Container.Services.get<Db>(Symbols.Infrastructure.DB);
        await db.init();
        console.log("World Started!");
    }

    static async closeAll(): Promise<void> {
        console.log("Stopping the world!");
        const db = Container.Services.get<Db>(Symbols.Infrastructure.DB);
        await db.closeAll();
        console.log("World stopped!");
    }
}
