import { Connection, createConnection, Model, Promise, Schema } from "mongoose";

export interface DbFactory {
    connect(): Promise<DbFactory>;
    link(schemas: { [key: string]: Schema }): any;
    disconnect(): Promise<void>;
}

export class DbFactoryImpl implements DbFactory {
    private readonly name: string;
    private connection!: Connection;
    private readonly url: string;

    constructor(url: string, name: string) {
        this.url = url;
        this.name = name;
    }

    async connect(): Promise<DbFactory> {
        return new Promise((resolve, reject) => {
            this.connection = createConnection(this.url, {
                dbName: this.name,
            });
            this.connection.once("open", () => {
                console.log("Connected to db ", this.name);
                resolve(this);
            });
        });
    }

    link(schemas: { [key: string]: Schema }): any {
        const models: any = {};
        for (const [name, schema] of Object.entries(schemas)) {
            models[name] = this.connection.model(name, schema);
        }
        return models;
    }

    async disconnect(): Promise<void> {
        await this.connection.close();
    }
}
