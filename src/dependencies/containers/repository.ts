import { ContainerModule, interfaces } from "inversify";
import { Symbols } from "../symbols";
import { UserRepository, UserRepositoryImpl } from "../../repository/user";
import { QuestionRepository, QuestionRepositoryImpl } from "../../repository/question";
import { QuizRepository, QuizRepositoryImpl } from "../../repository/quiz";
import { MessageRepository, MessageRepositoryImpl } from "../../repository/message";

export const RepositoryContainerModule = new ContainerModule((bind: interfaces.Bind) => {
    bind<UserRepository>(Symbols.Repository.User).to(UserRepositoryImpl);
    bind<QuestionRepository>(Symbols.Repository.Question).to(QuestionRepositoryImpl);
    bind<QuizRepository>(Symbols.Repository.Quiz).to(QuizRepositoryImpl);
    bind<MessageRepository>(Symbols.Repository.Message).to(MessageRepositoryImpl);
});
