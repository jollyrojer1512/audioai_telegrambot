import { inject, injectable } from "inversify";
import { Symbols } from "../dependencies/symbols";
import { QuestionEntity } from "../domain/entities/question";
import { QuestionRepository } from "../repository/question";
import { QuestionCategory } from "../components/models/question";

export interface QuestionService {
    getById(id: string): Promise<QuestionEntity>;
    getByIndex(index: number): Promise<QuestionEntity>;
    getNextByIndexAndType(index: number, category: QuestionCategory): Promise<QuestionEntity>;
}

@injectable()
export class QuestionServiceImpl implements QuestionService {
    constructor(@inject(Symbols.Repository.Question) private readonly questionRepository: QuestionRepository) {}

    async getById(id: string): Promise<QuestionEntity> {
        const question = await this.questionRepository.getById(id);
        if (!question) return undefined;
        return question;
    }
    async getByIndex(index: number): Promise<QuestionEntity> {
        const question = await this.questionRepository.getByIndex(index);
        if (!question) return undefined;
        return question;
    }
    async getNextByIndexAndType(index: number, category: QuestionCategory): Promise<QuestionEntity> {
        const question = await this.questionRepository.getNextByIndexAndType(index, category);
        if (!question) return undefined;
        return question;
    }

    private async updateOne(question: QuestionEntity): Promise<QuestionEntity> {
        return await question.save();
    }
}
