import { inject, injectable } from "inversify";
import { Context, InlineKeyboard, Keyboard as BuiltinKeyboard } from "grammy";
import { Symbols } from "../dependencies/symbols";
import { TextService } from "./text";
import { QuestionService } from "./question";
import { ComposerAnswers, QuestionAnswers, QuizAnswers } from "../components/util/enums";
import { QuestionCategory } from "../components/models/question";
import { QuizService } from "./quiz";
import { GrammyUser } from "../components/util/types";

export interface KeyboardService {
    startQuiz(): Promise<KeyboardAnswer>;
    question(index: number, category: QuestionCategory): Promise<KeyboardAnswer>;
    finishQuiz(user: GrammyUser): Promise<InlineKeyboard>;
    menu(ctx: Context): Promise<void>;
    settings(ctx: Context): Promise<void>;
}

export type KeyboardAnswer = {
    text: string;
    data?: {
        reply_markup: InlineKeyboard | BuiltinKeyboard;
    };
};

@injectable()
export class KeyboardServiceImpl implements KeyboardService {
    constructor(
        @inject(Symbols.Service.Text) private readonly text: TextService,
        @inject(Symbols.Service.Question) private readonly questionService: QuestionService,
        @inject(Symbols.Service.Quiz) private readonly quizService: QuizService
    ) {}

    async startQuiz(): Promise<KeyboardAnswer> {
        const menu = new InlineKeyboard();
        const data = `${ComposerAnswers.quiz}:${QuizAnswers.start}`;
        menu.text("Boshladikmi?", data);
        return {
            text: this.text.startQuiz,
            data: {
                reply_markup: menu,
            },
        };
    }

    async question(index: number, category: QuestionCategory): Promise<KeyboardAnswer> {
        const question = await this.questionService.getNextByIndexAndType(index, category);
        if (!question) return { text: this.text.finishQuiz };

        const text = question.question.value + "\nBu savol beradigan ballar soni: " + question.question.points;
        const menu = new InlineKeyboard();
        const data = `${ComposerAnswers.question}:${QuestionAnswers.skip}:${category}:${question.index}`;
        menu.text("o'tkazib yuborish", data);
        const result: KeyboardAnswer = {
            text,
            data: {
                reply_markup: menu,
            },
        };

        return result;
    }

    async finishQuiz(user: GrammyUser): Promise<InlineKeyboard> {
        const quiz = await this.quizService.getByUserId(user.id);
        return new InlineKeyboard()
            .text(`To'g'ri: ${quiz.questions.correct}`)
            .text(`Noto'g'ri: ${quiz.questions.wrong}`)
            .row()
            .text(`Umumiy ballar: ${quiz.points}`);
    }

    async menu(ctx: Context): Promise<void> {
        const menu = this.createMainMenu();
        const text = "menu";

        await ctx.reply(text, {
            reply_markup: {
                keyboard: menu.build(),
            },
        });
    }
    async settings(ctx: Context): Promise<void> {
        const settings = this.createSettingsMenu();

        await ctx.reply("settings", {
            reply_markup: {
                keyboard: settings.build(),
            },
        });
    }

    private createMainMenu(): BuiltinKeyboard {
        const menu = new BuiltinKeyboard();
        menu.text("menu").row();
        menu.text("settings").text("help").row();
        return menu;
    }

    private createSettingsMenu(): BuiltinKeyboard {
        const settings = new BuiltinKeyboard();
        settings.text("menu").row();
        settings.text("<-").text("->").row();
        return settings;
    }
}
