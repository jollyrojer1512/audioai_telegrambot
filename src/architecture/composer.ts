import { Bot as Grammy, Context } from "grammy";
import { inject, injectable } from "inversify";
import { Symbols } from "../dependencies/symbols";
import { Bot } from "./bot";
import { TextService } from "../service/text";
import { PrivateChat } from "./privateChat";
import { ComposerAnswers, QuestionAnswers, QuizAnswers } from "../components/util/enums";
import { KeyboardAnswer, KeyboardService } from "../service/keyboard";
import { QuestionCategory } from "../components/models/question";
import { GrammyUser } from "../components/util/types";
import { QuizService } from "../service/quiz";
import { QuizEntity } from "../domain/entities/quiz";

export interface Composer {
    listen(): Promise<void>;
}

export type ReplyType = KeyboardAnswer;

@injectable()
export class ComposerImpl implements Composer {
    private grammy: Grammy;

    constructor(
        @inject(Symbols.Architecture.Bot) private readonly BotContainer: Bot,
        @inject(Symbols.Architecture.PrivateChat) private readonly privateChat: PrivateChat,
        @inject(Symbols.Service.Text) private readonly text: TextService,
        @inject(Symbols.Service.Keyboard) private readonly keyboard: KeyboardService,
        @inject(Symbols.Service.Quiz) private readonly quizService: QuizService
    ) {
        this.grammy = BotContainer.bot;
    }

    async listen(): Promise<void> {
        this.grammy.on("callback_query", this.onAnswer.bind(this));
    }

    private async onAnswer(ctx: Context): Promise<void> {
        const data = ctx.callbackQuery.data.split(":");
        let result;
        switch (data[0] as ComposerAnswers) {
            case ComposerAnswers.question:
                result = await this.questionQuery(data, ctx.from);
                await this.updateTest(ctx.from, parseInt(data[3]));
                break;
            case ComposerAnswers.quiz:
                result = await this.quizQuery(data, ctx.from);
                break;
        }

        await this.ctxReply(ctx, result);
    }

    private async ctxReply(ctx: Context, reply: ReplyType): Promise<void> {
        await ctx.deleteMessage();
        await ctx.reply(reply.text, reply.data);
    }

    private async questionQuery(data: string[], user: GrammyUser): Promise<KeyboardAnswer> {
        let result: KeyboardAnswer;
        switch (data[1] as QuestionAnswers) {
            case QuestionAnswers.skip:
                await this.quizService.setIndexByUserId(user.id, parseInt(data[3]));
                result = await this.questionSkip(data);
        }
        if (result.text === this.text.finishQuiz)
            result.data = {
                reply_markup: await this.keyboard.finishQuiz(user),
            };
        return result;
    }

    private async questionSkip(data: string[]): Promise<KeyboardAnswer> {
        const category = data[2] as QuestionCategory;
        const index = parseInt(data[3]);
        const keyboard = await this.keyboard.question(index, category);
        return keyboard;
    }

    private async quizQuery(data: string[], user: GrammyUser): Promise<KeyboardAnswer> {
        let quiz: QuizEntity;
        switch (data[1] as QuizAnswers) {
            case QuizAnswers.start:
                quiz = await this.quizService.create(user.id);
                break;
        }
        return await this.keyboard.question(quiz.questions.index, quiz.questions.category);
    }

    private async updateTest(user: GrammyUser, index: number): Promise<void> {
        await this.quizService.setIndexByUserId(user.id, index);
    }
}
