import { inject, injectable } from "inversify";
import { Symbols } from "../dependencies/symbols";
import { UserRepository } from "../repository/user";
import { User } from "../components/models/user";
import { UserEntity } from "../domain/entities/user";
import { GrammyUser } from "../components/util/types";

export interface UserService {
    create(user: GrammyUser): Promise<UserEntity>;
    getByUserId(id: number): Promise<UserEntity>;
}

@injectable()
export class UserServiceImpl implements UserService {
    constructor(@inject(Symbols.Repository.User) private readonly userRepository: UserRepository) {}

    async create(grammyUser: GrammyUser): Promise<UserEntity> {
        const user = this.createUserObject(grammyUser);
        const userId = parseInt(user.user_id);
        const entity = await this.getByUserId(userId);
        if (!entity) return await this.userRepository.create(user);
        return await this.updateOne(entity);
    }

    async getByUserId(id: number): Promise<UserEntity> {
        const user = await this.userRepository.getByUserId(id);
        if (!user) return undefined;
        return user;
    }

    private createUserObject(user: GrammyUser): User {
        return {
            user_id: user.id.toString(),
            personal: {
                username: user.username,
                first_name: user.first_name,
                last_name: user.last_name,
            },
            register_time: new Date(),
        };
    }
    private async updateOne(user: UserEntity): Promise<UserEntity> {
        return await user.save();
    }
}
