import { Chat, User } from "@grammyjs/types/manage";
import { Voice, Message } from "@grammyjs/types/message";

export type GrammyUser = User;
export type GrammyVoice = Voice;
export type GrammyMessage = Message;
export type GrammyChat = Chat;
