import { BaseEntity } from "./baseEntity";
import { Message } from "../../components/models/message";
import { GrammyChat, GrammyUser, GrammyVoice } from "../../components/util/types";

export class MessageEntity extends BaseEntity<Message> {
    get registerTime(): Date {
        return this.model.register_time;
    }

    set registerTime(value: Date) {
        this.model.register_time = value;
    }

    get messageId(): string {
        return this.model.message_id;
    }

    set messageId(value: string) {
        this.model.message_id = value;
    }

    get chat(): GrammyChat {
        return this.model.chat;
    }

    set chat(value: GrammyChat) {
        this.model.chat = value;
    }

    get from(): GrammyUser {
        return this.model.from;
    }

    set from(value: GrammyUser) {
        this.model.from = value;
    }

    get voice(): GrammyVoice {
        return this.model.voice;
    }

    set voice(value: GrammyVoice) {
        this.model.voice = value;
    }
}
