export enum ComposerAnswers {
    question = "question",
    quiz = "quiz",
}

export enum QuestionAnswers {
    skip = "skip",
}

export enum QuizAnswers {
    start = "start",
}
