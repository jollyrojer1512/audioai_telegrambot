import { ContainerModule, interfaces } from "inversify";
import { Symbols } from "../symbols";
import { UserService, UserServiceImpl } from "../../service/user";
import { TextService, TextServiceImpl } from "../../service/text";
import { KeyboardService, KeyboardServiceImpl } from "../../service/keyboard";
import { QuestionService, QuestionServiceImpl } from "../../service/question";
import { QuizService, QuizServiceImpl } from "../../service/quiz";
import { MessageService, MessageServiceImpl } from "../../service/message";
import { MemoryService, MemoryServiceImpl } from "../../service/memory";

export const ServiceContainerModule = new ContainerModule((bind: interfaces.Bind) => {
    bind<UserService>(Symbols.Service.User).to(UserServiceImpl);
    bind<QuestionService>(Symbols.Service.Question).to(QuestionServiceImpl);
    bind<QuizService>(Symbols.Service.Quiz).to(QuizServiceImpl);
    bind<MessageService>(Symbols.Service.Message).to(MessageServiceImpl);

    bind<TextService>(Symbols.Service.Text).to(TextServiceImpl);
    bind<KeyboardService>(Symbols.Service.Keyboard).to(KeyboardServiceImpl);
    bind<MemoryService>(Symbols.Service.Memory).to(MemoryServiceImpl);
});
