import { User } from "../components/models/user";
import { inject, injectable } from "inversify";
import { Symbols } from "../dependencies/symbols";
import { Db } from "../components/db/db";
import { BaseRepository } from "./base";
import { UserEntity } from "../domain/entities/user";

export interface UserRepository {
    create(user: User): Promise<UserEntity>;
    getByUserId(userId: number): Promise<UserEntity>;
    updateOne(user_id: number, $set: any): Promise<void>;
}

@injectable()
export class UserRepositoryImpl extends BaseRepository<User, UserEntity> implements UserRepository {
    constructor(@inject(Symbols.Infrastructure.DB) private readonly db: Db) {
        super();
        super.init(this.db.main.user, UserEntity);
    }

    async create(model: User): Promise<UserEntity> {
        return super.create(model);
    }

    async updateOne(user_id: number, $set: any): Promise<void> {
        await super.update({ user_id: user_id.toString() }, { $set });
    }

    async getByUserId(id: number): Promise<UserEntity> {
        const user = await super.getOne({ user_id: id.toString() });
        return user;
    }
}
