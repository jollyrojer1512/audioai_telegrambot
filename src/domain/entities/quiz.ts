import { BaseEntity } from "./baseEntity";
import { Quiz, QuizPeriod, QuizQuestions } from "../../components/models/quiz";

export class QuizEntity extends BaseEntity<Quiz> {
    get userId(): string {
        return this.model.user_id;
    }
    set userId(value: string) {
        this.model.user_id = value;
    }

    get points(): number {
        return this.model.points;
    }
    set points(value: number) {
        this.model.points = value;
    }

    get period(): QuizPeriod {
        return this.model.period;
    }
    set period(value: QuizPeriod) {
        this.model.period = value;
    }

    get questions(): QuizQuestions {
        return this.model.questions;
    }
    set questions(value: QuizQuestions) {
        this.model.questions = value;
    }
}
