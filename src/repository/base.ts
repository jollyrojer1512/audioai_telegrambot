import { FilterQuery, HydratedDocument, Model, UpdateQuery } from "mongoose";
import { inject, injectable } from "inversify";
import { Symbols } from "../dependencies/symbols";
import { Db } from "../components/db/db";

export interface IBaseRepository<M, E> {
    init(model: Model<M>, entity: { new (doc: HydratedDocument<M>) }): void;
    getOne(filter: FilterQuery<M>): Promise<E>;
    get(filter: FilterQuery<M>): Promise<E[]>;
    create(model: M): Promise<E>;
    update(filter: FilterQuery<M>, update: UpdateQuery<M>): Promise<void>;
}

@injectable()
export class BaseRepository<M, E> implements IBaseRepository<M, E> {
    private model: Model<M>;
    private entity: { new (doc: HydratedDocument<M>) };

    init(model: Model<M>, entity: { new (doc: HydratedDocument<M>) }): void {
        this.model = model;
        this.entity = entity;
    }

    async getOne(filter: FilterQuery<M>): Promise<E> {
        const doc = await this.model.findOne(filter);
        if (!doc) return null;
        return new this.entity(doc);
    }

    async get(filter: FilterQuery<M>): Promise<E[]> {
        const docs = await this.model.find(filter);
        return docs.map((doc) => {
            return new this.entity(doc);
        });
    }

    async create(model: M): Promise<E> {
        const doc = await this.model.create(model);
        return new this.entity(doc);
    }

    async update(filter: FilterQuery<M>, update: UpdateQuery<M>): Promise<void> {
        await this.model.updateOne(filter, update);
    }
}
