import { Schema, SchemaTypes } from "mongoose";

export type Personal = {
    first_name: string;
    last_name: string;
    username: string;
};

export interface User {
    user_id: string;
    personal: Personal;
    register_time: Date;
}

export const userSchema = new Schema<User>({
    user_id: { type: String },
    personal: { type: SchemaTypes.Mixed },
    register_time: { type: Date },
});
