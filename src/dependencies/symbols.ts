export const Symbols = {
    // INFRASTRUCTURE
    Infrastructure: {
        Config: Symbol("Config"),
        DB: Symbol("DB"),
    },

    // ARCHITECTURE
    Architecture: {
        Bot: Symbol("Bot"),
        Composer: Symbol("Composer"),
        PrivateChat: Symbol("PrivateChat"),
    },

    // Service
    Service: {
        User: Symbol("UserService"),
        Question: Symbol("QuestionService"),
        Quiz: Symbol("QuizService"),
        Message: Symbol("MessageService"),
        Text: Symbol("TextService"),
        Keyboard: Symbol("KeyboardService"),
        Memory: Symbol("MemoryService"),
    },

    // Repository
    Repository: {
        User: Symbol("UserRepository"),
        Question: Symbol("QuestionRepository"),
        Quiz: Symbol("QuizRepository"),
        Message: Symbol("MessageRepository"),
    },
};
