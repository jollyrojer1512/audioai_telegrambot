import { ContainerModule, interfaces } from "inversify";
import { Symbols } from "../symbols";
import { PrivateChat, PrivateChatImpl } from "../../architecture/privateChat";
import { Bot, BotImpl } from "../../architecture/bot";
import { Composer, ComposerImpl } from "../../architecture/composer";

export const ArchitectureContainerModule = new ContainerModule((bind: interfaces.Bind) => {
    bind<Bot>(Symbols.Architecture.Bot).to(BotImpl).inSingletonScope();
    bind<PrivateChat>(Symbols.Architecture.PrivateChat).to(PrivateChatImpl);
    bind<Composer>(Symbols.Architecture.Composer).to(ComposerImpl);
});
