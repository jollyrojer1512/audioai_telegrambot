import "reflect-metadata";
import * as sm from "source-map-support";

sm.install();

import { Container } from "./dependencies/container";
import { Symbols } from "./dependencies/symbols";
import { Bot } from "./architecture/bot";
import { Composer } from "./architecture/composer";
import { InlineKeyboard } from "grammy";

async function start(): Promise<void> {
    Container.loadAll();
    await Container.init();

    const composer = Container.Services.get<Composer>(Symbols.Architecture.Composer);
    composer.listen();

    const bot = Container.Services.get<Bot>(Symbols.Architecture.Bot);
    const privateChat = bot.privateChat;

    // test();

    // bot.bot.on("message:photo", (ctx) => console.log(ctx));

    privateChat.command("start", async (ctx) => bot.start(ctx));

    privateChat.on("message:text", async (ctx) => bot.on(ctx));

    privateChat.on("message:voice", async (ctx) => bot.audio(ctx));
}

setImmediate(start);

async function test(): Promise<void> {
    const composer = Container.Services.get<Composer>(Symbols.Architecture.Composer);
    composer.listen();

    const bot = Container.Services.get<Bot>(Symbols.Architecture.Bot);
    const privateChat = bot.privateChat;

    privateChat.command("start", async (ctx) => {
        const num = new InlineKeyboard().text("Share Contact");
    });

    privateChat.on("message:contact", (ctx) => console.log(ctx));
}

async function stop(event: string): Promise<void> {
    // eslint-disable-next-line no-console
    console.log(`Incoming Signal: ${event}`);
    await Container.closeAll();

    console.log("Bot service end");
    process.exit(0);
}

// GRACEFULL SHUTDOWN
//process.stdin.resume(); //so the program will not close instantly

//catches ctrl+c event
process.on("SIGINT", stop.bind(null, "SIGINT"));
process.on("SIGTERM", stop.bind(null, "SIGTERM"));

process.on("unhandledRejection", (reason, promise) => {
    // eslint-disable-next-line no-console
    console.log("Unhandled Rejection at:", promise, "reason:", reason);
});
