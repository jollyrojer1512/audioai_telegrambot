import { HydratedDocument, Types } from "mongoose";

export class BaseEntity<T> {
    protected model: HydratedDocument<T>;
    constructor(model: HydratedDocument<T>) {
        this.model = model;
    }

    public async save(): Promise<any> {
        return this.model.save();
    }

    public async toObject(): Promise<any> {
        return this.model.toObject();
    }

    get id(): string {
        return this.model.id;
    }
    set id(value: string) {
        this.model.id = new Types.ObjectId(value);
    }
}
