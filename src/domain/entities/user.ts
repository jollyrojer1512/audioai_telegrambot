import { BaseEntity } from "./baseEntity";
import { Personal, User } from "../../components/models/user";

export class UserEntity extends BaseEntity<User> {
    get personal(): Personal {
        return this.model.personal;
    }

    set personal(value: Personal) {
        this.model.personal = value;
    }

    get registerTime(): Date {
        return this.model.register_time;
    }

    set registerTime(value: Date) {
        this.model.register_time = value;
    }

    get userId(): string {
        return this.model.user_id;
    }

    set userId(value: string) {
        this.model.user_id = value;
    }
}
