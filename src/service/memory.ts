import { inject, injectable } from "inversify";
import { Symbols } from "../dependencies/symbols";
import { Db, Memory } from "../components/db/db";
import { Config } from "../components/config/config";
import { Context } from "grammy";
import { QuizService } from "./quiz";
import { QuestionService } from "./question";

export interface MemoryService {
    listen(): Promise<void>;
    publish(message: string): Promise<void>;
    publishAudio(ctx: Context): Promise<void>;
}

export type AudioOutput = {
    file_id: string;
    user_id: string;
    quiz_id: string;
};
export type AudioInput = {
    result: string;
    user_id: string;
    quiz_id: string;
};

@injectable()
export class MemoryServiceImpl implements MemoryService {
    private readonly memory: Memory;
    private readonly channel: string;

    constructor(
        @inject(Symbols.Infrastructure.Config) private readonly config: Config,
        @inject(Symbols.Infrastructure.DB) private readonly db: Db,
        @inject(Symbols.Service.Quiz) private readonly quizService: QuizService,
        @inject(Symbols.Service.Question) private readonly questionService: QuestionService
    ) {
        this.memory = this.db.memory;
        this.channel = this.config.channel;
    }

    async publish(message: string): Promise<void> {
        await this.memory.publish(this.channel, message);
    }
    async publishAudio(ctx: Context): Promise<void> {
        const audio = await this.createAudioObject(ctx);
        await this.memory.publish(this.channel, audio.toString());
    }

    async listen(): Promise<void> {
        await this.connect();
        this.memory.on("message", (channel, message) => {
            this.onEvent(message);
        });
    }

    private async connect(): Promise<void> {
        await this.memory.subscribe(this.channel, (err, count) => {
            if (this.channel.length !== count) {
                console.log(`Failed to connect to ${this.channel}`);
            }
            if (err) {
                console.log(`Error: ${err?.message}`);
            } else {
                console.log(`Connected successfully to ${this.channel} channel!`);
            }
        });
    }

    private async onEvent(message: string): Promise<void> {
        const input: AudioInput = JSON.parse(message);
        const quiz = await this.quizService.getById(input.quiz_id);
        const question = await this.questionService.getByIndex(quiz.questions.index);

        const result = question.question.answer === input.result;
        if (result) quiz.points += question.question.points;
        else quiz.points -= question.question.points;

        await this.quizService.updateOne(quiz);
    }

    private async createAudioObject(ctx: Context): Promise<AudioOutput> {
        const quiz = await this.quizService.getByUserId(ctx.from.id);
        return {
            file_id: ctx.message.voice.file_id,
            quiz_id: quiz.id,
            user_id: ctx.from.id.toString(),
        };
    }
}
