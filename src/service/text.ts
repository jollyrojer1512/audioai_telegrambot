import { injectable } from "inversify";

export interface TextService {
    start: string;
    startQuiz: string;
    finishQuiz: string;
}

@injectable()
export class TextServiceImpl implements TextService {
    get start(): string {
        return "Salom";
    }
    get startQuiz(): string {
        return "Kettik";
    }
    get finishQuiz(): string {
        return "boshqa savol qolmadi!\nYeg'ilgan ballar:";
    }
}
