import { Schema, SchemaTypes } from "mongoose";
import { GrammyChat, GrammyUser, GrammyVoice } from "../util/types";

export interface Message {
    message_id: string;
    register_time: Date;
    chat: GrammyChat;
    from: GrammyUser;
    text?: string;
    voice?: GrammyVoice;
}

export const messageSchema = new Schema<Message>({
    message_id: { type: String },
    register_time: { type: Date },
    chat: { type: SchemaTypes.Mixed },
    from: { type: SchemaTypes.Mixed },
    text: { type: String },
    voice: { type: SchemaTypes.Mixed },
});
